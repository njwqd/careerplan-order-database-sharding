
package com.migrate.module.enumeration;

/**
 * 操作结果枚举值
 * 
 * @author zhonghuashishan
 */
public enum OperateResult
{
    /**
     * 失败：1
     */
    FAILED ("失败", "1"), 
    /**
     * 成功：0
     */
    SUCCESS ("成功", "0");
    
    private final String name;
    private final String value;

    OperateResult(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    /**
     * 取得枚举类型的值
     * 
     * @return 枚举类型的值
     */
    public String getValue ()
    {
        return this.value;
    }

    /**
     * 取得枚举类型的名称
     * 
     * @return 枚举类型的名称
     */
    public String getName ()
    {
        return this.name;
    }

    /**
     * 根据枚举类型的值取得枚举类型
     * 
     * @param typeValue 枚举类型的值
     * @return 枚举类型
     */
    public static OperateResult findByValue (String typeValue)
    {
        for (OperateResult operateResult : values ())
        {
            if (operateResult.getValue ().equals (typeValue))
            {
                return operateResult;
            }
        }
        return null;
    }

    /**
     * 根据枚举类型的值取得枚举类型的名称
     * 
     * @param typeValue 枚举类型的值
     * @return 枚举类型的名称
     */
    public static String getNameByValue (String typeValue)
    {
        for (OperateResult operateResult : values ())
        {
            if (operateResult.getValue ().equals (typeValue))
            {
                return operateResult.getName ();
            }
        }
        return null;
    }
}
