package com.migrate.module.domain;

import lombok.Data;

/**
 * 滚动信息
 *
 * @author zhonghuashishan
 */
@Data
public class ScrollInfo {

    /**
     * 本次同步的最大滚动ID
     */
    private String maxScrollId;
    /**
     * 本次同步的数据量
     */
    private Integer ScrollSize;
}
