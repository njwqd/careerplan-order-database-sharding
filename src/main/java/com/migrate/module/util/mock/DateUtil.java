package com.migrate.module.util.mock;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * 日期工具类
 * @Author Aurora
 * @Date 2021-11-29
 */
public class DateUtil {

    /**
     * 获取当前日期加上一个时间范围后的时间
     * @param number
     * @return
     */
    public static Timestamp addMinutes(int number) {
        return add(Calendar.MINUTE, number);
    }

    /**
     * 获取当前日期加上一个时间范围后的时间
     * @param number
     * @return
     */
    public static Timestamp addSecond(Date date,int number){
        return add(date,Calendar.SECOND,number);
    }

    /**
     * 获取当前日期加上一个时间范围后的时间
     * @param unit
     * @param number
     * @return
     */
    public static Timestamp add(int unit, int number) {
        return add(new Date(Calendar.getInstance().getTime().getTime()), unit, number);
    }

    /**
     * 获取某个日期加上一个时间范围的时间
     * @param left
     * @param unit
     * @param number
     * @return
     */
    public static Timestamp add(Date left, int unit, int number) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(left);
        instance.add(unit, number);
        return new Timestamp(instance.getTimeInMillis());
    }

}
